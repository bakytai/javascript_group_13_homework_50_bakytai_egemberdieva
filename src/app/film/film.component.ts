import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-film',
  templateUrl: './film.component.html',
  styleUrls: ['./film.component.css']
})
export class FilmComponent {
  @Input() title = '';
  @Input() year = '';
  @Input() image = '';
}
